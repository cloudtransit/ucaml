#!/usr/bin/env ruby

require 'sinatra'

set :bind, '0.0.0.0'
set :port, 80

get '/is-prime/:nr' do
  begin
    nr = Integer(params['nr'])
    divisors = (2..nr / 2).select { |i| nr % i == 0 }
    if divisors.empty?
      return "#{ nr } is a prime number"
    else
      return "#{ nr } is not a prime number. It can be divided by #{ divisors * ', ' }"
    end
  rescue
    return 500, "#{params['nr']} is not a number"
  end
end

get '/health' do
  'OK'
end