#!/usr/bin/env ruby

require 'sinatra'

set :bind, '0.0.0.0'
set :port, 8000

get '/env/:env' do
  begin
    if ENV[params['env']].nil? then return "not set" else return ENV[params['env']] end
  end
end

get '/cmd' do
  begin
    return 'parameter'
  end
end

get '/write/:filename/:content' do
  begin

    Dir.mkdir('/data') unless Dir.exists?('/data')
    dir = '/data/' + params['filename']
    File.write(dir,  params['content'])

    return File.file?(dir) ? "Datei #{params['filename']} erstellt " : "Fehler"
  end
end

get '/read/:filename' do
  begin
    dir = '/data/' + params['filename']
    content = File.read(dir) if File.file?(dir)
    return content.nil? ? "File not found #{params['filename']}" : content
  end
end

get '/health' do
  if ENV['dockerfile_env'].nil? then puts "In Dockerfile set environment variable missing" else 'OK' end
end

get '/' do
  return "UCAML (Unified Cloud Application Modeling Language) is a domain specific language (DSL) to define cloud applications being deployable on elastic container platforms like Kubernetes and Docker Swarm Mode. This service is designed for unit testing the DSL. You can \n\n List the Enviroment variables with /env/"
end