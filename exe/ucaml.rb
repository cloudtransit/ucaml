#!/usr/bin/env ruby

require 'commander/import'
require 'ucaml'
require 'ucaml/platforms'

program :name, Ucaml::NAME
program :version, Ucaml::VERSION
program :summary, Ucaml::SUMMARY
program :description, Ucaml::DESCRIPTION

program :help, 'Authors', 'N. Kratzke, P.-C. Quint, {kratzke, quint}@fh-luebeck.de'

default_command :help
always_trace!  # Comment this out for releases
# never_trace! # Comment this in for releases

command :transform do |c|
  c.syntax = "#{Ucaml::CLI} transform <files>+"
  c.description = 'Transform UCAML application definition files into container platform specific formats (swarm | k8s'
  c.option '--prime-service', 'Use Prime Service example'
  c.option '--guestbook', 'Use Guestbook example'
  c.option '--sock-shop', 'Use Sock-Shop example'
  c.option '--k8s', '(Default) Use Kuberentes as target container platform'
  c.option '--docker', 'Use Docker Swarm as target container platform'

  c.action do |files, options|

    files = [File.expand_path('../../data/prime-service.ucaml', __FILE__)] if options.prime_service
    files = [File.expand_path('../../data/guestbook.ucaml', __FILE__)] if options.guestbook
    files = [File.expand_path('../../data/sock-shop.ucaml', __FILE__)] if options.sock_shop

    files.each do |file|
      begin
        content = File.read(file)
        app = eval(content)
        m2m = Ucaml::Target::K8s.new(app.build) unless options.docker
        m2m = Ucaml::Target::Swarm.new(app.build) if options.docker
        say(m2m.build)
      rescue Exception => ex
        say "Could not transform file due to #{ex}"
        puts(ex.backtrace)
        exit(1)
      end
    end
  end
end

command :print do |c|
  c.syntax = "#{Ucaml::CLI} show <file>"
  c.description = 'Prints an UCAML application definition file in JSON or YAML format'
  c.option '--prime-service', 'Use Prime Service example'
  c.option '--guestbook', 'Use Guestbook example'
  c.option '--sock-shop', 'Use Sock-Shop example'

  c.option '-j', '--json', 'Print as JSON'
  c.option '-y', '--yaml', 'Print as YAML'
  c.action do |file, options|
    file = File.expand_path('../../data/prime-service.ucaml', __FILE__) if options.prime_service
    file = File.expand_path('../../data/guestbook.ucaml', __FILE__) if options.guestbook
    file = File.expand_path('../../data/sock-shop.ucaml', __FILE__) if options.sock_shop

    begin
      content = File.read(file)
      app = eval(content)
      say(app.as_yaml) if options.yaml
      say(app.as_json) if options.json
      say(app.as_yaml) unless options.yaml or options.json
    rescue Exception => ex
      say "Could not transform file due to #{ex}"
      puts(ex.backtrace)
      exit(1)
    end
  end
end