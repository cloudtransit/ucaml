
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ucaml/version"

Gem::Specification.new do |spec|
  spec.name          = "ucaml"
  spec.version       = Ucaml::VERSION
  spec.authors       = ["Nane Kratzke, Peter-Christian Quint"]
  spec.email         = ["nane.kratzke@fh-luebeck.de", "peter-christian.quint@fh-luebeck.de"]

  spec.summary       = "Unified cloud application modeling language"
  spec.description   = spec.summary
  spec.homepage      = "https://to.be.done"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = ['ucaml.rb']
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "yard", "~> 0.9"
  spec.add_development_dependency 'minitest', '~> 5.10'
  spec.add_development_dependency 'simplecov', '~> 0.15'

  spec.add_dependency 'rschema', '~> 3.2'
  spec.add_dependency 'commander', '~> 4.4'
  spec.add_dependency 'retriable', '~> 3.1'

end
