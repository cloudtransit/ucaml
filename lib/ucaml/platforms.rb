require 'json'
require 'yaml'

module Ucaml

  # Basic class to extend specific platform transformers.
  # Each transformer takes the UCAML format generated via {Ucaml.application} and transforms it
  # into a platform specific application definition format
  #
  # - e.g. concept manifest files for Kubernetes (JSON, YAML)
  # - e.g. compose files for Swarm (YAML)
  #
  # @author Nane Kratzke
  #
  class Transformer

    # Constructor to create an universal model to platform specific model transformer.
    #
    # @param data [Hash] An executable master architecture of a cloud application defined via {Ucaml.application}
    # @param validate [Boolean] If set, the input data is preprocessed and validated
    #
    def initialize(data, validate=true)
      @data = data
      @propagated = Marshal.load(Marshal.dump(data))
      if validate
        @propagated = self.preprocess(data)
        result = Ucaml::application(:schema).validate(@propagated)
        unless result.valid?
          puts(JSON.pretty_generate(result.error))
          exit(1)
        end
      end
    end

    # Propagates common settings of cloud application definitions down to lower level runtime concepts.
    # After this has been done, the cloud application definition can be transformed into
    # elastic container platform specific target formats using {build}.
    #
    # @param application [Hash] A hash describing the executable master architecture of a cloud application (defined via {Ucaml.application})
    #
    # @return [Hash] cloud application definition with transformable runtime concepts
    #
    def preprocess(application)

      app = Marshal.load(Marshal.dump(application)) # deep cloning

      # Propagate from application down to services
      for attr in [:labels, :scale, :ports, :environment, :alive, :ready, :scheduling, :request, :volumes]
        self.propagate(app, [], attr, [:services])
      end

      # Propagate from services down to containers
      for attr in [:labels, :scale, :ports, :environment, :alive, :ready, :scheduling, :request, :volumes]
        self.propagate(app, [:services], attr, [:container])
      end

      app
    end

    # Selects specific parts of nested Hash/Array structures using a selector (chain of keys).
    # <code>select</code> works similar like CSS selectors work on DOM-Trees.
    #
    # @param data [Hash, Array] An arbitray nested data structure (formed of Hashs, Arrays, Symbols and basic Ruby data types like Integer, String, Float, Boolean, ...)
    # @param selector [Array<Symbol>] List of selected attributes (chain of keys)
    #
    # @return [Array] List of selected parts that have been selected by *selector* from *data*
    #
    # @example To understand how selectors are working, it might be the best to study the unit test cases defined in {Ucaml.Tests.SelectorTests}. However, here is a short example.
    #   data = {
    #     a: {
    #       b: 42,
    #       c: 'Hello World'
    #     },
    #     b: [
    #       {
    #         nested: 'value 1'
    #       },
    #       {
    #         nested: 'value 2'
    #       }
    #     ]
    #   }
    #
    #   result = select(data, [:a, :b])      // => [42]
    #   result = select(data, [:b, :nested]) // => ['value 1', 'value 2']
    #   result = select(data, [:b])          // => [{ nested: 'value 1' }, { nested: 'value 2'}]
    #
    def select(data, selector)
      return [] if data.nil?
      return [data] if selector.empty?
      path = selector.clone
      entry = path.shift
      return data[entry].map { |e| select(e, path) }.flatten.compact if data[entry].is_a?(Array)
      select(data[entry], path).flatten.compact
    end

    # Merges parts of a complex data structure into lower level destinations of this data structure.
    # Used for data propagation in cloud application definition formats and to simplify and prepare M2M transformations.
    #
    # @param data [Hash, Array<Hash>, #write] The data structure to be propagated (this data structured is changed by this call!!!)
    # @param from [Array<Symbol>] A selector (as used by {#select}) to select the parts of data that should be propagated
    # @param attr [Symbol] The attribute to be propagated from selected parts
    # @param to [Array<Symbol, String>] A selector (as uses by {#select}) to select the destinations where selected parts of data should be merged into (this selector is relative to *from*)
    #
    # @return [void]
    #
    # @example To understand how propagate is working, it might be the best to study the unit test cases defined in {Ucaml.Tests.PropagateTests}.  However, this small example might be helpful at first. It shows how propagate can be used to define default settings in an outer scope and inject this default settings into lower level definitions without overwriting more specific settings).
    #   data = {
    #     setting: {
    #       value: 'default',
    #       optional: true
    #     },
    #     concepts: [
    #       {
    #         payload: 42,
    #         setting: { value: 'specific' },
    #       },
    #       {
    #         payload: 7,
    #         setting: { optional: false },
    #       },
    #     ]
    #   }
    #
    #   propagate(data, [], :setting, [:concepts]) // would result into the following propagation result of data
    #
    #   {
    #     setting: {
    #       value: 'default',
    #       optional: true
    #     },
    #     concepts: [
    #       {
    #         payload: 42,
    #         setting: { value: 'specific', optional: true },
    #       },
    #       {
    #         payload: 7,
    #         setting: { value: 'default', optional: false },
    #       },
    #     ]
    #   }
    #
    def propagate(data, from, attr, to)
      for origin in select(data, from)
        for target in select(origin, to)
          unless origin[attr].nil?
            target[attr] = origin[attr] if target[attr].nil?
            target[attr] = origin[attr].merge(target[attr]) if target[attr].is_a?(Hash) and origin[attr].is_a?(Hash)
          end
        end
      end
    end

    # Returns the universal cloud application definition format as JSON or YAML String, or as Hash.
    # This data has to be transformed into a platform specific format.
    #
    # @param as [Symbol] Indicate whether return should be in JSON, YAML or as hash (one of: <code>:hash, :json, :yaml</code>)
    #
    # @return [String, Hash] JSON/YAML representation or as Hash (depending on parameter *as*)
    #
    def universal(as: :hash)
      return Concept.new(@data).as_yaml if as == :yaml
      return Concept.new(@data).as_json if as == :json
      @data
    end

    # Returns the preprocessed universal cloud application definition format as JSON or YAML or as Hash.
    # General definitions have been propagated down to lower level concepts.
    # The rules how this down propagation work are defined by the function {#preprocess}.
    #
    # @param (see #universal)
    #
    # @return (see #universal)
    #
    def preprocessed(as: :hash)
      return Concept.new(@propagated).as_yaml if as == :yaml
      return Concept.new(@propagated).as_json if as == :json
      @propagated
    end

    # Returns the model to model transformation result.
    # This is the core part that must be implemented by a platform specific transformer.
    # @abstract
    #
    def build()
      raise("to be implemented")
    end

  end

  require 'ucaml/k8s'
  require 'ucaml/swarm'
  # This module contains all model to model {Ucaml.Transformer}s to generate container platform specific
  # cloud application definition formats.
  #
  module Target

  end

end