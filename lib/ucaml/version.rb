module Ucaml

  # UCAML name (for CLI)
  #
  NAME = 'UCAML'

  # UCAML summary (for CLI)
  #
  SUMMARY = 'UCAML CLI'

  # UCAML description (for CLI)
  #
  DESCRIPTION = 'Unified Cloud Application Modeling Language - Command Line Interface'

  # UCAML version
  #
  VERSION = '0.3.0'

  # UCAML CLI program
  #
  CLI = 'ucaml-cli.rb'

end
