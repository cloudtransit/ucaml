module Ucaml
  module Target

    # The Kubernetes transformer.
    # Transforms Ucaml executable master architectures into K8s manifests.The following
    # Kubernetes concepts are generated.
    #
    # - Deployment[https://kubernetes.io/docs/concepts/workloads/controllers/deployment/]
    # - Service[https://kubernetes.io/docs/concepts/services-networking/service/]
    # - Namespace[https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/]
    # - Horizontal-Pod-Autoscaler[https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/]
    #
    # @author Nane Kratzke and Peter-Christian Quint
    #
    class K8s < Transformer

      # Generates a Kubernetes namespace.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @return [Concept] Namespace manifest
      #
      def namespace(app)
        Concept.new(
            apiVersion: 'v1',
            kind: 'Namespace',
            metadata: { name: "#{app.name}-ns" }
        )
      end

      # Generates a Kubernetes deployment manifest.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @param svc [Concept] a service of *app* defined via a {Ucaml.service} generator.
      # @return [Concept] Deployment manifest
      #
      def deployment(app, svc)

        container = Concept.new(
            name: svc.container.name,
            image: svc.container.image,
            ports: svc.container.ports.map { |p| { containerPort: p }},
            resources: {
                limits: { cpu: "#{svc.container.request.cpu}m", memory: "#{svc.container.request.memory}M",  },
                requests: { cpu: "#{svc.container.request.cpu}m", memory: "#{svc.container.request.memory}M",  }
            }
        )

        container.update(:volumeMounts => svc.volumes.collect{|x| {:mountPath => x.mountPath, :name => x.name.to_s}}) if !svc.volumes.nil? and svc.volumes.is_a?(Array)

        container.update(command: svc.container.cmd) unless svc.container.cmd.nil?

        # include optional parameters if they are available
        # environment
        container.update({
                            env: svc.container.environment.build.collect{|k, v| {:name => k.to_s, :value => v.to_s }}
                        }) if !svc.container.environment.nil?

        labels = svc.labels.nil? ? {container: svc.container.name} : svc.labels.build

        manifest = Concept.new(
            apiVersion: 'apps/v1',
            kind: 'Deployment',
            metadata: {
                name: "#{svc.container.name}",
                namespace: "#{app.name}-ns",
            },
            spec: {
                selector: { matchLabels: labels},
                template: {
                    metadata: { labels: labels },
                    spec: { containers: [container] }
                }
            }
        )

        manifest.spec.template.metadata.update(
            labels: manifest.spec.template.spec.update(env: svc.environment.build)
        ) unless svc.environment.nil?


        manifest.update({
          spec: {
            template: {
                spec: {
                    :volumes => svc.volumes.collect{|x| {:name => x.name}}
                }
            }
          }
        })  if !svc.volumes.nil? and svc.volumes.is_a?(Array)



        manifest
      end

=begin DEPRECATED
      # Generates a Kubernetes ingress manifest.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @param svc [Concept] a service of *app* defined via a {Ucaml.service} generator.
      # @return [Concept] Ingress manifest, if service is exposed as public service by *app*
      # @return [nil] if service is not exposed as public service by *app*
      #
      def ingress(app, svc)
        return nil unless app.expose and app.expose.map { |e| e.service }.include?(svc.name)
        expose = app.expose.select { |e| e.service == svc.name }.first

        Concept.new(
            apiVersion: 'extensions/v1',
            kind: 'Ingress',
            metadata: {
                name: "#{svc.name}-ingress",
                namespace: "#{app.name}-ns",
                annotations: { 'ingress.kubernetes.io/rewrite-target' => '/' }
            },
            spec: {
                rules: [
                    { http: { paths: [ { path: "/#{expose.endpoint}", backend: { serviceName: svc.name, servicePort: svc.ports.first }} ] }}
                ]
            }
        )
      end
=end

      # Generates a Kubernetes service manifest.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @param svc [Concept] a service of *app* defined via a {Ucaml.service} generator.
      # @return [Concept] Service manifest
      #
      def service(app, svc)

        labels = svc.labels.nil? ? {container: svc.container.name} : svc.labels.build

        service = Concept.new(
            apiVersion: 'v1',
            kind: 'Service',
            metadata: {
                name: svc.name,
                namespace: "#{app.name}-ns",
                labels: labels
            },
            spec: {
                selector: labels,
                ports: svc.ports.map.with_index { |port, i|
                  (!svc.expose.nil? and !svc.expose.detect{|pair| pair[port]}.nil?) ? { port: port, name: "map-#{port}-to-#{svc.container.ports[i]}", targetPort: svc.container.ports[i], nodePort:  svc.expose.find{|pair| pair[port]}.build[port] } :  {port: port, name: "map-#{port}-to-#{svc.container.ports[i]}", targetPort: svc.container.ports[i]}}

            },
        )
        service.update(:spec => {type: "NodePort"}) unless svc.expose.nil?

        service
      end

      # Generates a Kubernetes horizontal pod autoscaler manifest.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @param svc [Concept] a service of *app* defined via a {Ucaml.service} generator.
      # @return [Concept] autoscaler manifest, if the service defines autoscaling rules
      # @return [nil] if the service does not define autoscaling rules
      #
      def hpa(app, svc)
        return nil if svc.container.scale.nil?

        Concept.new(
            apiVersion: 'autoscaling/v1',
            kind: 'HorizontalPodAutoscaler',
            metadata: {
                name: "#{svc.container.name}-scaler",
                namespace: "#{app.name}-ns"
            },
            spec: {
                scaleTargetRef: {
                    apiVersion: 'apps/v1',
                    kind: 'Deployment',
                    name: svc.container.name
                },
                minReplicas: svc.container.scale.min,
                maxReplicas: svc.container.scale.max,
                targetCPUUtilizationPercentage: svc.container.scale.targetCpu
            }
        )
      end

      # Generates all Kubernetes specific manifests.
      # @return [String] YAML string that contains all necessary manifests to launch the application via <code>kubectl apply</code>
      #
      def build
        app = Concept.new(self.preprocessed)
        manifests = [namespace(app)] + app.services.map { |svc| [
            service(app, svc),
          #  ingress(app, svc), # DEPRECATED
            deployment(app, svc),
            hpa(app, svc)]
        }
        manifests.flatten.compact.map { |manifest| manifest.as_yaml } * ''
      end
    end
  end
end