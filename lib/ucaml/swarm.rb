module Ucaml
  module Target

    # The Docker Swarm mode transformer.
    # Transforms Ucaml executable master architectures into Docker Swarm manifests. The following
    #
    #
    # @author Peter-Christian Quint
    #
    class Swarm < Transformer


      # Generates a Kubernetes service manifest.
      # @param app [Concept] an application defined via a {Ucaml.application} generator.
      # @param svc [Concept] a service of *app* defined via a {Ucaml.service} generator.
      # @return [Concept] Service manifest
      #
      def service(app, svc)

        service = Concept.new(image: svc.container.image)
        service.update(ports: svc.ports.map.with_index { |port, i|
          (!svc.expose.nil? and !svc.expose.detect{|pair| pair[port]}.nil?) ? svc.expose.find{|pair| pair[port]}.build[port].to_s+":" + port.to_s : svc.container.ports[i]}
        )
        service.update(environment: svc.container.environment.build.collect{|k, v| k.to_s + "=" + v.to_s}) unless svc.container.environment.nil?
        service.update(command: svc.container.cmd) unless svc.container.cmd.nil?

        #Requested ressources and number of replications
        deploy = Concept.new()
        deploy.update(resources: { reservations: {
            cpus: (Float(svc.container.request.cpu)/100).to_s,
            memory: svc.container.request.memory.to_s + "M"
        }}) unless svc.container.request.nil?
        deploy.update(resources: { reservations: { cpus: (Float(svc.container.scale.targetCpu)/100).to_s }}) unless svc.container.scale.targetCpu.nil?
        deploy.update(mode: "replicated", replicas: svc.container.scale.max) unless svc.container.scale.nil?
        service.update(deploy: deploy) unless deploy.build.empty?

        service
      end


      # Generates all Kubernetes specific manifests.
      # @return [String] YAML string that contains all necessary manifests to launch the application via <code>kubectl apply</code>
      #
      def build
        app = Concept.new(self.preprocessed)
        services={}
        app.services.each{|svc| services.update(svc.name.to_sym => service(app, svc))}
        Concept.new(version: "3", services: services).as_yaml
      end
    end
  end
end