require 'ucaml/version'
require 'ucaml/platforms'

require 'rschema'

# This module defines UCAML (Universal Cloud Application Modeling Language) and its concepts to
# define executable master architectures for cloud applications.
#
# UCAML can be used
#
# - to define UCAML models (executable master architectures for cloud applications), and
# - to process,
# - to validate,
# - and finally to transform
#
# these models via a model-to-model transformation into elastic container platform specific definition formats.
# Model-to-model transformations can be extended simply by extending the {Ucaml::Transformer} class. Right now,
# the following target container platforms are supported via the following transformers.
#
# - Kubernetes via {Target.K8s}
#
# @author Nane Kratzke
#
module Ucaml

  # This regular expression defines valid names in UCAML.
  # The naming convention has the intent to be used in DNS naming use cases.
  # That is a defensive approach that should work with all naming conventions of elastic container platforms.
  #
  # - Names can be build from the standard characters 'a' - 'z'.
  # - Names can have digits '0' - '9' in it, but it is not allowed to start a name with a digit.
  # - Whitespace characters are not allowed in names.
  # - If you want to separate several parts of a name, this can be done using '-'.
  #
  # Examples of valid names are:
  #
  # - "service-name" (this is not allowed in most programming languages, but should be no problem for container platforms)
  # - "service1"
  # - "service02"
  #
  # But not:
  #
  # - "Service" (has one capital letter)
  # - "1234service" (starts with a digit)
  # - "service?!" (only - is allowed as special characters)
  # - "this,is,no,name" (see above)
  # - "this is no name" (space and other whitespace characters are not allowed)
  #
  NAMING_CONVENTION = /\A[a-z\-]+[a-z\d_\-]*\z/


  # Convenience wrapper class for all UCAML concept objects
  # that have been generated with concept generator functions {application}, {service}, {container}, ... .
  # The settings of these objects can be accessed, read, modified and copied using this wrapper class.
  #
  # @author Nane Kratzke
  # @author Peter-Christian Quint (initiator)
  #
  # @example How to generate a concept from scratch
  #   concept = Concept.new.attrib1('val1').attrib2(42).attrib3(['A', 'short', 'list'])
  #   puts(concept.attrib1) # => val1
  #   puts(concept.attrib2) # => 42
  #   puts(concept.attrib3) # => [A, short, list]
  #
  #   # This is equivalent (take what is more convenient in your eyes)
  #   concept = Concept.new(attrib1: 'val1', attrib2: 42, attrib3: ['A', 'short', 'list'])
  #   puts(concept.attrib1) # => val1
  #   puts(concept.attrib2) # => 42
  #   puts(concept.attrib3) # => [A, short, list]
  #
  # @example We can modify concept objects
  #   concept = Concept.new(existing: 'value', further: 'VALUE')
  #   concept.further('changed') # overwrites further
  #   concept.another('setting') # adds another
  #   concept.update(existing: 'example', new: 'test') # overwrites existing, adds new
  #   puts(concept.build) # use build to get the representing hash of the concept
  #   {
  #     existing: 'example',
  #     further: 'changed',
  #     another: 'setting',
  #     new: 'test'
  #   }
  #
  # @example And we can use existing concept objects as a kind of template simply by copying and updating them.
  #   template = Concept.new(range: 'default', min: 1, max: 10)
  #   copy = template.copy.update(range: 'my range', max: 100)
  #   puts(template.build) # { range: 'default', min: 1, max: 10 }
  #   puts(copy.build) # { range: 'my range', min: 1, max: 100 }
  #
  # @example UCAML concept generators return that kind of {Concept} objects
  #   default_scaling = Ucaml::scalingrule(min: 1, max: 10, cpu: 75)
  #   aggressive_scaling = default_scaling.copy.update(max: 100, cpu: 25)
  #   aggressive_scaling.min(30)
  #   puts(default_scaling) # { min: 1, max: 10, cpu: 75 }
  #   puts(aggressive_scaling) # { min: 10, max: 100, cpu: 25 }
  #
  class Concept

    # @!attribute [r,w,rw] definition
    # @private
    # @return [Hash] the current concept setting
    attr_accessor :definition

    # Constructor to create a {Concept} object.
    # @param settings [Hash] detail settings
    #
    def initialize(settings={})
      @definition = settings
    end

    # Converts all hash objects of a nested hash or array into {Concept} objects.
    # @private
    # @param data [Hash, Array, Object] data structure
    # @return [Array<Concept>] Array of Concept objects, if *data* is an array of hash objects
    # @return [Array<Object>] unchanged array, if *data* is an array of non-hash objects
    # @return [Concept] Concept object, if *data* is a hash
    # @return [Object] object without change, otherwise
    #
    def convert(data)
      return data.map { |e| convert(e) } if data.is_a?(Array)
      return Concept.new(data.map { |k, v| [k, convert(v)] }.to_h) if data.is_a?(Hash)
      data
    end

    # Defines equality on {Concept} objects.
    # @return [Boolean] <code>true</code> if classes of objects are equal and their internal definition (nested hash/array)
    # @return [Boolean] <code>false</code>, otherwise
    #
    def ==(other)
      self.class == other.class && self.definition == other.definition
    end

    # Returns or updates the value of the current concept setting to which *name* is mapped.
    # @param name [Symbol] Called method (this *name* will be used as key in the concept setting)
    # @param args [Array] Method parameters passed.
    #   - Only the first parameter is evaluated.
    #   - If args is a <code>Hash</code>, this parameter is merged with the current value stored for *name*.
    #   - If args is no <code>Hash</code>, this parameter replaces the current value stored for *name*
    #
    # @return [Object] Value for the key *name* in the concept setting (might be <code>nil</code> if *name* is not present)
    # @return [Concept] if the method has been used to update the concept setting (this can be used for method chaining)
    #
    def method_missing(name, *args)
      value = args.first

      if value.nil?
        return convert(self.definition[name]) if self.definition[name].is_a?(Hash)
        return convert(self.definition[name]) if self.definition[name].is_a?(Array)
        return self.definition[name]
      end

      # We have an update method, do we really need an attribute setter behaviour here?
      #
      if self.definition[name].nil?
        self.definition[name] = value
        return self
      end

      if self.definition[name].is_a?(Hash) and value.is_a?(Hash)
        self.definition[name].merge!(value)
        return self
      end

      self.definition[name] = value
      self
    end

    # Updates the concept setting.
    # @param updates [Hash]
    # @return [Concept] Reference to the {Concept} object itself. Useful for method chaining.
    #
    def update(updates={})

      updates = updates.build if updates.is_a?(Concept)

      merger = proc { |_, a, b|

          a = a.definition if a.is_a?(Concept)
          b = b.build if b.is_a?(Concept)

          if (a.is_a?(Hash)  && b.is_a?(Hash))
            a.merge(b, &merger)
          else
            if (a.is_a?(Array) && b.is_a?(Array))
              a | b
            else
              [:undefined, nil, :nil].include?(b) ? a : b
            end
          end
        }
      self.definition.merge!(updates.to_h, &merger)
      self
    end

    # Returns a deep copy of the {Concept} object.
    # @return [Concept] Deep copy of the {Concept} object.
    #
    def copy
      copy = Marshal.load(Marshal.dump(self.definition))
      Concept.new(copy)
    end

    # Returns a hash representation of the {Concept} object.
    # @return [Hash] Nested hash that represents the concept object setting
    #
    def build
      ret = {}
      for key in self.definition.keys
        value = self.definition[key]
        ret[key] = value unless value.is_a?(Hash) or value.is_a?(Array) or value.is_a?(Concept)
        ret[key] = value.build if value.is_a?(Concept)
        ret[key] = Concept.new(value).build if value.is_a?(Hash)
        if value.is_a?(Array)
          ret[key]  = []
          ret[key] += value.select { |v| v.is_a?(Concept) }.map { |v| v.build }
          ret[key] += value.select { |v| v.is_a?(Hash) }.map { |v| Concept.new(v).build }
          ret[key] += value.reject { |v| v.is_a?(Hash) or v.is_a?(Concept) }
        end
      end
      ret
    end

    # Returns a JSON representation of the {Concept} object.
    # @return [String] JSON string
    #
    def as_json
      JSON.pretty_generate(self.build)
    end

    # Deep stringifying all key symbols in Hashes (:symbol => 'symbol').
    # Helpful to create a readable YAML representation.
    #
    # @param data [Hash] The hash for that all key symbols shall be stringified
    # @return [Hash] The hash where all key symbols are stringified
    #
    # @private
    #
    def _stringify_all_keys(data)
      return data.map { |entry| _stringify_all_keys(entry) } if data.is_a?(Array)
      return data unless data.is_a?(Hash)

      stringified_hash = {}
      data.each do |k, v|
        stringified_hash[k.to_s] = _stringify_all_keys(v)
      end
      stringified_hash
    end

    # Returns a YAML representation of the {Concept} object.
    # @return [String] YAML string
    #
    def as_yaml
      data = self.copy.build
      YAML.dump(self._stringify_all_keys(data))
    end
  end

  # Defines the application concept of UCAML.
  # This is the top level concept of every executable cloud application macro-architecture.
  # An application is formed of one or more {service}s.
  #
  # @param name [String] The name of the application
  #
  # @option details [String] :namespace The namespace for the application
  # @option details [Array<Hash>] :services A list of service definitions, each defined via {service}
  # deprecated: @option details [Array<String>] :expose A list of service names that should be exposed to public
  # @option details (see service)
  #
  # @return [Concept] A {Concept} object describing the executable master architecture of a cloud application
  # @return [RSchema] A schema to validate application settings, if called as <code>Ucaml::application(:schema)</code>
  #
  def self.application(name, details={})
    return RSchema.define_hash {{
        name: pipeline(_String, predicate('name') { |n| n.match(Ucaml::NAMING_CONVENTION)}),
        services: array(Ucaml::service(:schema)),
        #optional(:expose) => array(Ucaml::expose(:schema)),
        optional(:volumes) => array(Ucaml::volume(:schema)),
        optional(:labels) => Ucaml::labels(:schema),
        optional(:scheduling) => Ucaml::constraint(:schema),
        optional(:environment) => Ucaml::environment(:schema),
        optional(:request) => Ucaml::request(:schema),
        optional(:scale) => Ucaml::scalingrule(:schema),
        optional(:alive) => Ucaml::probe(:schema),
        optional(:ready) => Ucaml::probe(:schema),
    }} if name == :schema

    services = []
    services = details[:services] unless details[:services].nil?

    description = {
        name: name,
        services: services
    }
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
    #description
  end

  # Defines the service concept of UCAML.
  # A service is part of an {application} and is composed of {container}s.
  #
  # @param name [String] The name of the service
  #
  # @option details (see container)
  #
  # @return [Concept] A {Concept} object describing a service of a cloud application
  # @return [RSchema] A schema to validate service settings, if called as <code>Ucaml::service(:schema)</code>
  #
  def self.service(name, details={})

    return RSchema.define_hash {{
        name: pipeline(_String, predicate('name') { |n| n.match(Ucaml::NAMING_CONVENTION)}),
        ports: array(pipeline(_Integer, predicate('port') { |p| p.between?(1, 65535)})),
        container: Ucaml::container(:schema),
        optional(:volumes) => array(Ucaml::volume(:schema)),
        optional(:labels) => Ucaml::labels(:schema),
        optional(:environment) => Ucaml::environment(:schema),
        optional(:request) => Ucaml::request(:schema),
        optional(:scale) => Ucaml::scalingrule(:schema),
        optional(:scheduling) => Ucaml::constraint(:schema),
        optional(:alive) => Ucaml::probe(:schema),
        optional(:ready) => Ucaml::probe(:schema),
        optional(:capabilities) => Ucaml::constraint(:schema),
        optional(:expose) => array(variable_hash((pipeline(_Integer, predicate{ |p| p.between?(1, 65535)})) => (pipeline(_Integer, predicate{ |p| p.between?(1, 65535)})))),
    }} if name == :schema

    description = {
        name: name,
    }
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
  end

=begin DEPRECATED

  # Defines and service exposition concept of UCAML.
  # A service can be exposed to the outside world by assigning a service port to a service endpoint.
  #
  # @param from [String] The name of the service that should be exposed (this must be the same name as defined by {service} constructor)
  # @param port [Integer] The port number of the service that should be exposed
  # @param as [String] The name of the endpoint. Under that name the service can accessed from the outside world.
  #
  # @return [Concept] A {Concept} object describing a service exposition
  # @return [RSchema] A schema to validate service exposition settings, if called as <code>Ucaml::expose(:schema)</code>
  #
  def self.expose(schema = "", from: nil, port: nil, as: nil)
    return RSchema.define_hash {{
        service: pipeline(_String, predicate('name') { |n| n.match(Ucaml::NAMING_CONVENTION)}),
        port: pipeline(_Integer, predicate('port') { |p| p.between?(1, 65535)}),
        endpoint: pipeline(_String, predicate('endpoint') { |n| n.match(Ucaml::NAMING_CONVENTION)})
    }} if schema == :schema

    Concept.new(
        service: from,
        port: port,
        endpoint: as
    )
  end
=end

  # Defines the container concept of UCAML.
  # A container is an executable runtime concept that can be executed by an elastic container platform.
  # A container is the unit of execution of a {service}.
  #
  # @param name [String] The name of the container
  # @param image [String] The image of the container
  #
  # @option details [Array] :ports Exposed port numbers of a container
  # @option details [String] :cmd Shell command to be executed by container
  # @option details [Hash] :labels Assigned labels (key-value pairs) defined via {labels}
  # @option details [Hash] :environment Environment variables defined via {environment}
  # @option details [Hash] :alive Liveliness checking probe defined via {probe}
  # @option details [Hash] :ready Readiness checking probe defined via {probe}
  #
  # @return [Concept] A {Concept} object describing a container
  # @return [RSchema] A schema to validate container settings, if called as <code>Ucaml::container(:schema)</code>
  #
  def self.container(name, image='', details={})
    return RSchema.define_hash {{
        name: pipeline(_String, predicate('name') { |n| n.match(Ucaml::NAMING_CONVENTION)}),
        image: _String,
        request: Ucaml::request(:schema),
        optional(:cmd) => array(_String),
        optional(:volumes) => array(Ucaml::volume(:schema)),
        optional(:ports) => array(pipeline(_Integer, predicate { |p| p.between?(1, 65535) })),
        optional(:labels) => Ucaml::labels(:schema),
        optional(:environment) => Ucaml::environment(:schema),
        optional(:alive) => Ucaml::probe(:schema),
        optional(:ready) => Ucaml::probe(:schema),
        optional(:scheduling) => Ucaml::constraint(:schema),
        optional(:scale) => Ucaml::scalingrule(:schema),
    }} if name == :schema

    description = {
        name: name,
        image: image
    }
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
  end

  # Defines the volume concept of UCAML.
  # A volume is basically a directory, possibly with some data in it, which is accessible to one or more containers.
  #
  # @param name [String] The name of the volume
  # @param type [Symbol] Type of the volume (one of :emptydir, :inmemory, :git, :nfs, :secret)
  #
  # @option details [Hash] :nfs NFS volume specific settings
  # @option details [Hash] :git GIT volume specific settings
  # @option details [Hash] :secret Volume specific settings to mount secrets
  # @option details [Hash] :mountPath container volume mount path
  # @return [Concept] A {Concept} object a volume mountable by a container
  # @return [RSchema] A schema to validate volume settings, if called as <code>Ucaml::volume(:schema)</code>
  #
  def self.volume(name, type=:emptydir, details={})

    return RSchema.define_hash {{
        name: pipeline(_String, predicate('name') { |n| n.match(Ucaml::NAMING_CONVENTION)}),
        type: pipeline(_Symbol, predicate('type') { |t| [:emptydir, :nfs, :git, :inmemory, :secret].include?(t) }),
        optional(:nfs) => _Hash,
        optional(:git) => _Hash,
        optional(:secret) => _Hash,
        optional(:mountPath) => _String
    }} if name == :schema

    description = {
        name: name,
        type: type
    }
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
  end

  # Defines the probe concept of UCAML.
  # A probe is used by elastic container platforms to check periodically whether a container is ready or alive.
  #
  # @param type [String] probetype one of ('httpget') (list will be extended)
  #
  # @option details [String] :path path (e.g. '/health')
  # @option details [Integer] :port port (1 < port < 65535, e.g. 80)
  # @option details [Integer] :delay delay in seconds (delay > 0, e.g. 60)
  # @option details [Integer] :period period in seconds (period > 0, e.g. 30)
  #
  # @return [Concept] A {Concept} object describing a probe setting
  # @return [RSchema] A schema to validate probe settings, if called as <code>Ucaml::probe(:schema)</code>
  #
  def self.probe(type, details={})

    return RSchema.define_hash {{
        probetype: pipeline(_String, predicate('probetype') { |t| ['httpget'].include?(t) }),
        path: pipeline(_String),
        port: pipeline(_Integer, predicate('port') { |port| port.between?(1, 65535) }),
        delay: pipeline(_Integer, predicate('delay') { |seconds| seconds > 0 }),
        period: pipeline(_Integer, predicate('period') { |seconds| seconds > 0 })
    }} if type == :schema

    description = {
        probetype: type
    }
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
  end

  # Defines the request concept of UCAML.
  # Defines a resource request for a {container}.
  # Resource limits are used by container platforms to schedule containers to nodes in a resource efficient way.
  #
  # @option details [Integer] :cpu CPU request in Millicores (cpu > 0, e.g. 100 [m], which is a tenth of a CPU core)
  # @option details [Integer] :memory Memory request in Megabytes (memory > 0, e.g. 128 [MB])
  # @option details [Integer] :ephemeral_storage Storage request in Gigabytes (ephemeral_storage > 0, e.g. 2 [GB])
  #
  # @return [Concept] A {Concept} object describing a resource request
  # @return [RSchema] A schema to validate request settings, if called as <code>Ucaml::request(:schema)</code>
  #
  # @see https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-cpu Millicore
  #
  def self.request(details)

    return RSchema.define_hash {{
        cpu: pipeline(_Integer, predicate('cpu') { |cpu| cpu > 0 }), # Millicpu, e.g. 100 means 100m
        memory: pipeline(_Integer, predicate('memory') { |mem| mem > 0 }), # Megabytes, e.g. 64 means 64M
        optional(:ephemeral_storage) => pipeline(_Integer, predicate('storage') { |gb| gb > 0 }), # Gigabytes, e.g. 2 means 2G
    }} if details == :schema

    description = {}
    description.merge!(details) if details.is_a?(Hash)
    Concept.new(description)
  end

  # Defines the auto-scaling rule concept of UCAML.
  # Auto scaling rules are used by container platforms to launch additional or to terminate unnecessary
  # {container}s according to metric.
  #
  # @option details [Integer] :min minimum amount of deployment units launched at any given time (min > 0, default: 1)
  # @option details [Integer] :max maximum amount of deployment units launched at any given time (max > min, default: 10)
  # @option details [Integer] :cpu average target CPU utilization in percent (0 <= cpu <= 100, default: 50)
  #
  # @return [Concept] A {Concept} object describing a scaling rule for deployment units
  # @return [RSchema] A schema to validate scaling settings, if called as <code>Ucaml::scalingrule(:schema)</code>
  #
  def self.scalingrule(details)

    return RSchema.define_hash {{
        min: pipeline(_Integer, predicate('min') { |min| min > 0 }),
        max: pipeline(_Integer, predicate('max') { |max| max > 0 }),
        optional(:targetCpu) => pipeline(_Integer, predicate('cpu') { |cpu| cpu.between?(0, 100) })
    }} if details == :schema

    details[:min] = 1 if details[:min].nil?
    details[:max] = 10 if details[:max].nil?
    details[:cpu] = 50 if details[:cpu].nil?

    description = {
        min: details[:min],
        max: details[:max],
        targetCpu: details[:cpu]
    }
    Concept.new(description)
  end

  # Defines the scheduling constraint concept of UCAML.
  # Scheduling constraints are considered by schedulers of elastic container platforms to place {container}s on nodes.
  # A scheduling constraint is basically a list of key-value pairs that must be all fulfilled.
  #
  # @param kvpairs [Hash] Key-value pairs forming the constraints
  #
  # @return [Concept] A {Concept} object describing constraints being considered when scheduling a container
  # @return [RSchema] A schema to validate constraints, if called as <code>Ucaml::constraint(:schema)</code>
  #
  def self.constraint(kvpairs)
    return RSchema.define { variable_hash(_Symbol => _String) } if kvpairs == :schema
    description = {}
    description.merge!(kvpairs) if kvpairs.is_a?(Hash)
    Concept.new(description)
  end

  # Defines the environment variables concept of UCAML.
  # Environment variables can be injected by container platforms into {container}s to provide
  # necessary configuration data for a {container}.
  #
  # @param kvpairs [Hash] Key-value pairs forming the environment variables
  #
  # @return [Concept] A {Concept} object describing environment variables for containers
  # @return [RSchema] A schema to validate labels, if called as <code>Ucaml::environment(:schema)</code>
  #
  def self.environment(kvpairs)
    return RSchema.define { variable_hash(_Symbol => _String) } if kvpairs == :schema
    description = {}
    description.merge!(kvpairs) if kvpairs.is_a?(Hash)
    Concept.new(description)
  end

  # Defines a label concept of UCAML.
  # By using labels it is possible to annotate components of cloud applications using key-value pairs.
  # Labels are a list of key-value pairs that can be considered by container platforms for various purposes like:
  # scheduling constraints, assigning containers to services, load balancing.
  #
  # @param kvpairs [Hash] Key-value pairs
  #
  # @return [Concept] A {Concept} object describing labels (a list of key-value pairs)
  # @return [RSchema] A schema to validate labels, if called as <code>Ucaml::labels(:schema)</code>
  #
  def self.labels(kvpairs)
    return RSchema.define { variable_hash(_Symbol => _String) } if kvpairs == :schema
    description = {}
    description.merge!(kvpairs) if kvpairs.is_a?(Hash)
    Concept.new(description)
  end
end