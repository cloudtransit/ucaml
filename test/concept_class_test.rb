require 'minitest/autorun'
require 'ucaml'

module Ucaml
  module Tests

    # Tests the {Ucaml::Concept} wrapper class.
    #
    # @author Nane Kratzke and Peter-Christian Quint
    #
    class ConceptClassTests < Minitest::Test

      # Tests the empty concept creation with method chaining based value setting.
      #
      def test_empty_concept_creation
        concept = Concept.new.attrib1('val1').attrib2(42).attrib3(['A', 'short', 'list'])
        assert_equal('val1', concept.attrib1)
        assert_equal(42,concept.attrib2)
        assert_equal(['A', 'short', 'list'], concept.attrib3)
      end

      # Tests the common concept creation.
      #
      def test_concept_creation
        concept = Concept.new(attrib1: 'val1', attrib2: 42, attrib3: ['A', 'short', 'list'])
        assert_equal('val1', concept.attrib1)
        assert_equal(42,concept.attrib2)
        assert_equal(['A', 'short', 'list'], concept.attrib3)
      end

      # Tests the update of concept objects.
      #
      def test_concept_update
        concept = Concept.new(existing: 'value', further: 'VALUE')
        concept.further('changed') # overwrites further
        concept.another('setting') # adds another
        concept.update(existing: 'example', new: 'test') # overwrites existing, adds new

        hash = concept.build # use build to get the hash representation of the concept object

        assert_equal(4, hash.keys.size)
        assert_equal('example', hash[:existing])
        assert_equal('changed', hash[:further])
        assert_equal('setting', hash[:another])
        assert_equal('test', hash[:new])

        ##############################
        # Validate with empty values #
        ##############################

        concept = Concept.new()
        concept.update(lonely_entry: "value")

        assert_equal(1, concept.build.keys.size)
        assert_equal("value", concept.build[:lonely_entry])

        concept.update({})
        assert_equal(1, concept.build.keys.size)

        ###################################
        # Validate interleaved hash merge #
        ###################################
        concept_hash_interleaved = Concept.new(
            level_00_entry: {
                level_000_entry: {
                    level_0000_entry: 'level_0000_value',
                    level_0001_entry: 'level_0001_value'}
            },
            level_01_entry: {
                level_010_entry: 'level_010_value',
            },
            level_02_entry: 'level_02_value'
        )
        concept_hash_interleaved.update(
            level_00_entry:
                {
                    level_000_entry: {
                        level_0001_entry: 'level_0001_value_CHANGE',
                        level_0002_entry: 'level_0002_value_NEW'
                    }
            },
            level_01_entry: 'level_010_value_OVERWRITE_HASH',
            level_02_entry: 'level_02_value_CHANGE',
            level_03_entry: 'level_03_value_NEW'
        )

        hash = concept_hash_interleaved.build
        assert_equal(4, hash.keys.size) # level_0x entries

        level_000_entry_hash = hash[:level_00_entry][:level_000_entry]
        assert_equal(3, level_000_entry_hash.keys.size) # level_000x entries
        assert_equal('level_0000_value', level_000_entry_hash[:level_0000_entry]) # this did not changed
        assert_equal('level_0001_value_CHANGE', level_000_entry_hash[:level_0001_entry]) # this is changed
        assert_equal('level_0002_value_NEW', level_000_entry_hash[:level_0002_entry]) # this is new

        assert_equal('level_010_value_OVERWRITE_HASH', hash[:level_01_entry]) # this is new
        assert_equal('level_02_value_CHANGE', hash[:level_02_entry]) # this is new
        assert_equal('level_03_value_NEW', hash[:level_03_entry]) # this is new

        ################################
        # Validate inner Concept merge #
        ################################

        concept_concept_interleaved = Concept.new(
            level_00_entry: Concept.new(
                level_000_entry: Concept.new(
                    level_0000_entry: 'level_0000_value',
                    level_0001_entry: 'level_0001_value'
                )
            ),
            level_01_entry: Concept.new(
                level_010_entry: 'level_010_value',
            ),
        )
        concept_concept_interleaved.update(
            level_00_entry: Concept.new(
                  level_000_entry: Concept.new(
                      level_0001_entry: 'level_0001_value_CHANGE',
                      level_0002_entry: 'level_0002_value_NEW'
                  )
            ),
            level_01_entry: 'level_010_value_OVERWRITE_CONCEPT',
        )
        assert_equal(Ucaml::Concept, concept_concept_interleaved.level_00_entry.class) # test if still concept class
        assert_equal(Ucaml::Concept, concept_concept_interleaved.level_00_entry.level_000_entry.class) # test if still concept class

        hash = concept_concept_interleaved.build
        assert_equal(2, hash.keys.size) # level_0x entries
        level_000_entry_hash = hash[:level_00_entry][:level_000_entry]
        assert_equal(3, level_000_entry_hash.keys.size) # level_000x entries
        assert_equal('level_0000_value', level_000_entry_hash[:level_0000_entry]) # this did not changed
        assert_equal('level_0001_value_CHANGE', level_000_entry_hash[:level_0001_entry]) # this is changed
        assert_equal('level_0002_value_NEW', level_000_entry_hash[:level_0002_entry]) # this is new
        assert_equal('level_010_value_OVERWRITE_CONCEPT', hash[:level_01_entry]) # this is new

        ##########################################
        # Validate inner Concept merge with Hash #
        ##########################################

        concept_concept_hash_interleaved = Concept.new(
            level_00_entry: Concept.new(
                level_000_entry: Concept.new(
                    level_0000_entry: 'level_0000_value',
                    level_0001_entry: 'level_0001_value'
                )
            ),
        )
        concept_concept_hash_interleaved.update(
            level_00_entry:{
                    level_000_entry: {
                        level_0001_entry: 'level_0001_value_CHANGE',
                        level_0002_entry: 'level_0002_value_NEW'
                    }
                },
            level_01_entry: {level_010_entry: 'level_010_value_NEW'},
        )

        assert_equal(Ucaml::Concept, concept_concept_hash_interleaved.level_00_entry.class) # test if still concept class
        assert_equal(Ucaml::Concept, concept_concept_hash_interleaved.level_00_entry.level_000_entry.class) # test if still concept class

        hash = concept_concept_hash_interleaved.build
        assert_equal(2, hash.keys.size) # level_0x entries

        level_000_entry_hash = hash[:level_00_entry][:level_000_entry]
        assert_equal(3, level_000_entry_hash.keys.size) # level_000x entries
        assert_equal('level_0000_value', level_000_entry_hash[:level_0000_entry]) # this did not changed
        assert_equal('level_0001_value_CHANGE', level_000_entry_hash[:level_0001_entry]) # this is changed
        assert_equal('level_0002_value_NEW', level_000_entry_hash[:level_0002_entry]) # this is new
        level_01_entry_hash = hash[:level_01_entry]
        assert_equal('level_010_value_NEW', level_01_entry_hash[:level_010_entry]) # this is new

        ##########################################
        # Validate inner Concept merge with Hash #
        ##########################################

        concept_hash_concept_interleaved = Concept.new(
            level_00_entry:{
                level_000_entry: {
                    level_0000_entry: 'level_0000_value',
                    level_0001_entry: 'level_0001_value'
                }
            },
            level_01_entry: {level_010_entry: 'level_010_value_NEW'},
        )

        concept_hash_concept_interleaved.update(
            level_00_entry: Concept.new(
                level_000_entry: Concept.new(
                    level_0001_entry: 'level_0001_value_CHANGE',
                    level_0002_entry: 'level_0002_value_NEW'
                )
            ),
        )
        hash = concept_hash_concept_interleaved.build
        assert_equal(2, hash.keys.size) # level_0x entries

        level_000_entry_hash = hash[:level_00_entry][:level_000_entry]
        assert_equal(3, level_000_entry_hash.keys.size) # level_000x entries
        assert_equal('level_0000_value', level_000_entry_hash[:level_0000_entry]) # this did not changed
        assert_equal('level_0001_value_CHANGE', level_000_entry_hash[:level_0001_entry]) # this is changed
        assert_equal('level_0002_value_NEW', level_000_entry_hash[:level_0002_entry]) # this is new
        level_01_entry_hash = hash[:level_01_entry]
        assert_equal('level_010_value_NEW', level_01_entry_hash[:level_010_entry]) # this is new

        #TODO Array-Test
      end

      # Tests that concepts can be used in templating style.
      # Changes on copies do not change the original concept objects.
      #
      def test_concept_templating
        template = Concept.new(range: 'default', min: 1, max: 10)
        copy = template.copy.update(range: 'my range', max: 100)

        assert_equal('default', template.range)
        assert_equal(1, template.min)
        assert_equal(10, template.max)

        assert_equal('my range', copy.range)
        assert_equal(1, copy.min)
        assert_equal(100, copy.max)

        copy.min(10)
        assert_equal(1, template.min)
        assert_equal(10, copy.min)
      end

      # Tests that concepts can build valid hash representations.
      #
      def test_concept_hash_building
        test = Concept.new(
         range: 'default',
         int: 42,
         hash: {
           int: 42,
           array: [5, 2, 3, 4],
           concept: Concept.new(
             deep: 'test',
             array: [1, 2, 3, 4, 5]
           )
         },
         array: [
           Concept.new.a1(1).a2(4).a3([1, 2, 3]).a4(a: 'test1', concept: Concept.new(x: 1)),
           Concept.new.a1(2).a2(5).a3([3, 2, 1]).a4({a: 'test2', concept: Concept.new(x: 2)}),
           Concept.new.a1(3).a2(6).a3([1, 2]).a4({a: 'test3', concept: Concept.new({ x: 3 })}),
         ]
        ).build

        assert_equal(4, test.keys.size)
        assert_equal(42, test.dig(:hash, :int))
        assert_equal('test', test.dig(:hash, :concept, :deep))
        assert_equal([1,2,3,4,5], test.dig(:hash, :concept, :array))
        assert(test.dig(:hash, :concept).is_a?(Hash))
        assert(test.dig(:array).all? { |v| v.is_a?(Hash) })
        assert(test.dig(:array).all? { |v| v.dig(:a4, :concept).is_a?(Hash) })
        assert_equal(1, test.dig(:array).first.dig(:a4, :concept, :x))
        assert_equal(3, test.dig(:array).last.dig(:a4, :concept, :x))
        assert_equal(3, test.dig(:array).size)
      end
    end
  end
end