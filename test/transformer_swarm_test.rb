require 'minitest/autorun'
require 'ucaml'
require 'json'
require 'timeout'

module Ucaml
  module Tests

    # Tests the K8s transformer {Ucaml.Target.K8s}.
    #
    # @author Peter-Christian Quint and Nane Kratzke
    #
    class TransformerswarmTest < Minitest::Test
      # Prepares the test data structures for this test case.
      #
      def setup
          @testdata = Ucaml::application('test-service',
                                         services: [
                                             Ucaml::service('test-service',
                                                            request: Ucaml::request(cpu: 100, memory: 256, ephemeral_storage: 2),
                                                            scale: Ucaml::scalingrule(min: 1, max: 3, cpu: 66),
                                                            ports: [8000],
                                                            expose: [8000 => 32500], #range depends on minikube/k8s config. Default 30001-32767
                                                            container: Ucaml::container('test-unit', 'testservice:latest', cmd: ["ruby", "cmd_set.rb"])
                                             )
                                         ]
          )
      end


      # Tests whether the example prime-service application is generated correctly.
      #
      def test_test_service_manifest_generation
        transformer = Ucaml::Target::Swarm.new(@testdata.build)
        file = Tempfile.new
        file.write(transformer.build)
        file.close
        %x{docker stack rm #{@testdata.name}}
        sleep 2
        result = %x{docker stack deploy -c #{file.path} #{@testdata.name}}
        puts result
        assert(result.include?("Creating network #{@testdata.name}_default"))
        assert(result.include?("service #{@testdata.name}_#{@testdata.services.first.name}"))

        %x{docker stack rm #{@testdata.name}}
        file.delete
      end

    end
  end
end