require 'minitest/autorun'
require 'ucaml'

module Ucaml
  module Tests

    # Tests the UCAML schema validation implementation of {Ucaml.Transformer}.
    #
    # @author Nane Kratzke
    #
    class SchemaValidationTests < Minitest::Test

      # Prepares reference application definition formats
      #
      def setup
        target_cpu = Ucaml::scalingrule(min: 1, max: 10, cpu: 66)
        noscale = Ucaml::scalingrule(min: 1, max: 1, cpu: 100)

        @valid = Ucaml::application('my-app', {
            labels: Ucaml::labels(app: 'name', version: '0.0.1'),
            scale: target_cpu,
            services: [
                Ucaml::service('service', {
                    labels: Ucaml::labels(service: 'service', version: '0.0.2'),
                    ports: [1234],
                    scheduling: Ucaml::constraint('beta.kubernetes.io/os': 'linux'),
                    environment: Ucaml::environment(
                        ZIPKIN: 'zipkin.zipkin.svc.cluster.local',
                        JAVA_OPTS: '-Xms64m -Xmx128m -XX:PermSize=32m -XX:MaxPermSize=64m -XX:+UseG1GC -Djava.security.egd=file:/dev/urandom'
                    ),
                    request: Ucaml::request(cpu: 100, memory: 256, ephemeral_storage: 2),
                    scale: noscale,
                    container: Ucaml::container( 'container1', 'image1', request: Ucaml::request(cpu: 500), ports: [81], cmd: ['ruby hw-service.rb', 'ruby hw-service2.rb'])
                })
            ]
        })

        @unknown_key = Ucaml::application('my-invalid-app', {
            labels: Ucaml::labels(app: 'name', version: '0.0.1'),
            scale: target_cpu,
            services: [
                Ucaml::service('service', {
                    labels: Ucaml::labels(service: 'service', version: '0.0.2'),
                    ports: [1234],
                    scheduling: Ucaml::constraint('beta.kubernetes.io/os': 'linux'),
                    environment: Ucaml::environment(
                        ZIPKIN: 'zipkin.zipkin.svc.cluster.local',
                        JAVA_OPTS: '-Xms64m -Xmx128m -XX:PermSize=32m -XX:MaxPermSize=64m -XX:+UseG1GC -Djava.security.egd=file:/dev/urandom'
                    ),
                    undefined: 'key', # THIS IS AN UNKNOWN KEY (should be detected)
                    scale: noscale,
                    request: Ucaml::request(cpu: 100, memory: 256, ephemeral_storage: 2),
                    container: Ucaml::container( 'container1', 'image1', request: Ucaml::request(cpu: 500), ports: [81], cmd: ['ruby hw-service.rb', 'ruby hw-service2.rb']),
                })
            ]
        })

        @missing_key = Ucaml::application('my-invalid-app', {
            labels: Ucaml::labels(app: 'name', version: '0.0.1'),
            scale: target_cpu,
            services: [
                Ucaml::service('service', {
                    labels: Ucaml::labels(service: 'service', version: '0.0.2'),
                    ports: [1234],
                    scheduling: Ucaml::constraint('beta.kubernetes.io/os': 'linux'),
                    environment: Ucaml::environment(
                        ZIPKIN: 'zipkin.zipkin.svc.cluster.local',
                        JAVA_OPTS: '-Xms64m -Xmx128m -XX:PermSize=32m -XX:MaxPermSize=64m -XX:+UseG1GC -Djava.security.egd=file:/dev/urandom'
                    ),
                    scale: noscale,
                    container: Ucaml::container( 'container2', 'image2', labels: Ucaml::labels(service: 'sidecar')), # MISSING REQUEST
                })
            ]
        })

        @incomplete_setting = Ucaml::application('my-invalid-app', {
            labels: Ucaml::labels(app: 'name', version: '0.0.1'),
            scale: target_cpu,
            services: [
                Ucaml::service('service', {
                    labels: Ucaml::labels(service: 'service', version: '0.0.2'),
                    ports: [1234],
                    scheduling: Ucaml::constraint('beta.kubernetes.io/os': 'linux'),
                    environment: Ucaml::environment(
                        ZIPKIN: 'zipkin.zipkin.svc.cluster.local',
                        JAVA_OPTS: '-Xms64m -Xmx128m -XX:PermSize=32m -XX:MaxPermSize=64m -XX:+UseG1GC -Djava.security.egd=file:/dev/urandom'
                    ),
                    scale: noscale,
                    container: Ucaml::container( 'container1', 'image1', request: Ucaml::request(memory: 128), ports: [81], cmd: ['ruby hw-service.rb', 'ruby hw-service2.rb']), # INCOMPLETE REQUEST (cpu missing)
                })
            ]
        })
      end

      # Tests that validation works with valid application definitions.
      #
      def test_valid_app_definition
        valid = Marshal.load(Marshal.dump(@valid.build))
        transform = Ucaml::Transformer.new(valid, false)
        check = transform.preprocess(valid)
        result = Ucaml::application(:schema).validate(check)
        assert(result.valid?)
      end

      # Tests that validation detects invalid application definitions due to unspecified keys.
      #
      def test_unknown_key
        invalid = Marshal.load(Marshal.dump(@unknown_key.build))
        transform = Ucaml::Transformer.new(invalid, false)
        check = transform.preprocess(invalid)
        result = Ucaml::application(:schema).validate(check)
        assert(result.invalid?)
        assert([:undefined], result.error.dig(:services, 0).vars[:extraneous_keys])
      end

      # Tests that validation detects invalid application definitions due to mandatory keys.
      #
      def test_missing_key
        invalid = Marshal.load(Marshal.dump(@missing_key.build))
        transform = Ucaml::Transformer.new(invalid, false)
        check = transform.preprocess(invalid)
        result = Ucaml::application(:schema).validate(check)
        assert(result.invalid?)
        assert_equal([:request], result.error.dig(:services, 0, :container).vars[:missing_keys])
      end

      # Tests that validation detects invalid application definitions due to incomplete settings.
      #
      def test_incomplete_setting
        invalid = Marshal.load(Marshal.dump(@incomplete_setting.build))
        transform = Ucaml::Transformer.new(invalid, false)
        check = transform.preprocess(invalid)
        result = Ucaml::application(:schema).validate(check)
        assert(result.invalid?)
        assert_equal([:cpu], result.error.dig(:services, 0, :container, :request).vars[:missing_keys])
      end
    end
  end
end