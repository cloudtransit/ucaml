require 'minitest/autorun'
require 'ucaml'

module Ucaml
  module Tests

    # Tests the propagate implementation of {Ucaml.Transformer#propagate} class.
    #
    # @author Nane Kratzke
    #
    class PropagateTests < Minitest::Test

      # Prepares the test data structure for this test case.
      #
      def setup
        @testdata = {
            default: {
                string: 'default',
                int: 42
            },
            value: 42,
            detail: {
                default: {
                    string: 'more specific'
                },
                value: 42,
                specifics: [
                    {
                        string: 'most specific',
                        value: {}
                    },
                    {
                        other: 'value',
                        default: {
                            int: 42
                        }
                    }
                ],
                hash: {
                    string: 'specific'
                },
                existing: {
                    value: -42
                }
            }
        }
      end

      # Returns a deep clone of the test data.
      # @return [Hash, Array] clone of the nested test data structure
      #
      def clone_test_data
        Marshal.load(Marshal.dump(@testdata))
      end

      # Tests that propagate merges hashes in a way, that more general (top level) definitions
      # do not overwrite more specific (lower level) definitions.
      #
      def test_propagate_hash_merge
        data = self.clone_test_data
        propagator = Ucaml::Transformer.new(data,false)
        propagator.propagate(data, [], :default, [:detail])

        assert_equal(2, data[:detail][:default].keys.size)
        assert_equal('more specific', data[:detail][:default][:string])
        assert_equal(42, data[:detail][:default][:int])
      end

      # Tests that propagate merges values in a way, that more general (top level) definitions
      # do not overwrite more specific (lower level) definitions.
      #
      def test_propagate_value_merge
        data = self.clone_test_data
        propagator = Ucaml::Transformer.new(data,false)
        propagator.propagate(data, [:detail], :value, [:specifics])

        assert(data[:detail][:specifics].map { |e| e.key?(:value) }.all?)
        assert_equal({}, data[:detail][:specifics].first[:value])
        assert_equal(42, data[:detail][:specifics].last[:value])
      end

      # Tests that propagate merges hashes into every hash of an array.
      #
      def test_propagate_into_arrays
        data = self.clone_test_data
        propagator = Ucaml::Transformer.new(data,false)
        propagator.propagate(data, [:detail], :default, [:specifics])

        assert_equal(2, data[:detail][:specifics].size)
        assert(data[:detail][:specifics].map { |e| e.key?(:default) }.all?)
        assert(not(data[:detail][:specifics].first[:default].key?(:int)))
        assert_equal(42,data[:detail][:specifics].last[:default][:int])
      end

      # Tests that single values are inserted if not present.
      # Tests that single values are not inserted if already present in target.
      #
      def test_propagate_value_into_hash
        data = self.clone_test_data
        propagator = Ucaml::Transformer.new(data,false)
        propagator.propagate(data, [:detail], :value, [:hash])
        propagator.propagate(data, [:detail], :value, [:existing])

        assert_equal(2, data[:detail][:hash].keys.size)
        assert_equal('specific', data[:detail][:hash][:string])
        assert_equal(42, data[:detail][:hash][:value])

        assert_equal(1, data[:detail][:existing].keys.size)
        assert_equal(-42, data[:detail][:existing][:value])
      end
    end
  end
end