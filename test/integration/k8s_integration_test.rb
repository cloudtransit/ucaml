require 'minitest/autorun'
require 'ucaml'
require 'json'
require 'timeout'
require 'retriable'
require_relative '../environment/minikube_test'

module Ucaml

  # This module contains tests checking that some example and reference applications can be deployed successfully.
  # These example applications compromise:
  #
  # - guestbook application
  # - prime service application
  # - sock-shop application
  #
  module IntegrationTest

    # Tests the K8s transformer {Ucaml.Target.K8s}.
    #
    # @author Nane Kratzke
    #
    class IntegrationK8s < Minitest::Test

      include EnvironmentTest
      # Tests that the prime service example application effectively can be launched and is reachable.
      #
      def test_prime_service_launch
        ucaml = eval(File.read(File.expand_path('../../../data/prime-service.ucaml', __FILE__)))

        transformer = Ucaml::Target::K8s.new(ucaml.build)

        file = Tempfile.new
        file.write(transformer.build)
        file.close

        %x{kubectl apply -f #{file.path}}
        ip = %x{minikube ip}.strip

        prime = 'undefined'
        noprime = 'undefined'

        Retriable.retriable(tries: 3, base_interval: 10) do
          prime = %x{curl -k -s http://#{ip}:32501/is-prime/13}
          noprime = %x{curl -k -s http://#{ip}:32501/is-prime/123}
          raise('Service not up') unless prime.start_with?('13')
          raise('Service not up') unless noprime.start_with?('123')
        end

        assert(prime.start_with? "13 is a prime number")
        assert(noprime.start_with? "123 is not a prime number")
        %x{kubectl delete -f #{file.path}}
        file.delete
      end

    end
  end
end