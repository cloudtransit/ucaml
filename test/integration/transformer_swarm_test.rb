require 'minitest/autorun'
require 'ucaml'
require 'json'
require 'timeout'
require 'net/http'
require 'uri'
module Ucaml
  module Tests

    # Tests the K8s transformer {Ucaml.Target.K8s}.
    #
    # @author Peter-Christian Quint and Nane Kratzke
    #
    class TransformerswarmTest < Minitest::Test
      # Prepares the test data structures for this test case.
      #
      def setup
          @testdata = Ucaml::application('test-service',
                                         services: [
                                             Ucaml::service('test-service',
                                                            request: Ucaml::request(cpu: 100, memory: 256, ephemeral_storage: 2),
                                                            scale: Ucaml::scalingrule(min: 1, max: 3, cpu: 66),
                                                            ports: [8000],
                                                            expose: [8000 => 32500], #range depends on minikube/k8s config. Default 30001-32767
                                                            container: Ucaml::container('test-unit', 'testservice:latest', environment: {test_env_name: 'test_env_content'}, cmd: ["ruby", "cmd_set.rb"])
                                             )
                                         ]
          )
      end

      # Tests whether the example prime-service application is generated correctly.
      #
      def test_deploy_test


        transformer = Ucaml::Target::Swarm.new(@testdata.build)
        file = Tempfile.new
        file.write(transformer.build)
        file.close
        %x{docker stack rm #{@testdata.name}}
        sleep 2

        result = %x{docker stack deploy -c #{file.path} #{@testdata.name}}
        assert(result.include?("Creating network #{@testdata.name}_default"))
        service_name = "#{@testdata.name}_#{@testdata.services.first.name}"
        assert(result.include?("service #{service_name}"))
        sleep 5

        # Command - test
        result = %x{docker ps --filter "name=#{service_name}" --format "{{.Command}}"}
        assert(@testdata.services.first.container.cmd.join(' ').tr(result.chomp, '').empty?, "Set CMD parameter (#{@testdata.services.first.container.cmd.join(' ')}) not identical to existing (#{result.chomp})") # Test passed command parameter. Odd tr and empty is used because @testdata.services.first.container.cmd.join(' ').eqal(result.chomp) is always false

        #Enviroment - test
        env_key, env_value = @testdata.services.first.container.environment.build.first
        exp_key, exp_value = @testdata.services.first.expose.first.build.first
        uri = URI.parse("http://localhost:#{exp_value}/env/#{env_key}")
        result = Net::HTTP.get_response(uri).body
        assert_equal(result, env_value, "Env. variable #{env_key}: #{env_value} expected but got #{result}")
        %x{docker stack rm #{@testdata.name}}
        file.delete


      end

    end
  end
end