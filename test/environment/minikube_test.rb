require 'minitest/autorun'
require 'ucaml'
require 'json'
require 'timeout'

module Ucaml

  # This module contains tests checking that the local environment has all necessary services running for the transformer and integration tests.
  #
    module EnvironmentTest

      # Tests the K8s transformer {Ucaml.Target.K8s}.
      #
      # @author Peter-Christian Quint and Nane Kratzke
      #
      class MinikubeTest < Minitest::Test


        # Test necessary kubectl and minikube setup.
        # Necessary to validate generated manifest files.
        #
        def test_kubectl_and_minikube_setup
          err_msg_minikube = "Try 'minikube start --kubernetes-version v1.9.0'"
          begin
            Timeout::timeout(3){ # if minikube is not started, kubectl takes long time to response
              v =  YAML.load(%x{kubectl version -o yaml})

              # validate Kubectl version
              cv = v["clientVersion"]
              assert(!cv.nil?,  "Kubectl seems not to be installed or correct configured") # => also false if "kubectrl: command not found"
              assert_equal(1, cv["major"].to_i, "Kubectl in 1.x (x>=9) required")
              assert(cv["minor"].to_i >= 9, "Kubectl in version 1.x (x>=9) required")

              # validate Kubernetes version
              sv = v["serverVersion"]

              # TODO Currently the k8s 'major' and 'minor' version-information is not given using 'kubectl version'. So we use the 'gitVersion' for it. This should me changed later (see https://github.com/kubernetes/kubectl/issues/269)
              assert(!sv.nil?,  "Kubernetes not available. " + err_msg_minikube)
              gitVersion = sv["gitVersion"].scan(/\d/)
              assert_equal(1, gitVersion[0].to_i, "Kubernetes in version 1.x (x>=9) required. " + err_msg_minikube)
              assert(gitVersion[1].to_i >= 9, "Kubernetes in version 1.x (x>=9) required. " + err_msg_minikube)
              # assert_equal(1, sv["major"].to_i, "Kubernetes in version 1.x (x>=9) required. " + err_msg_minikube)
              # assert(sv["minor"].to_i >= 9, "Kubernetes in version 1.x (x>=9) required. " + err_msg_minikube)

            }
          rescue Timeout::Error
            raise(Timeout::Error,"Kubernetes not available. " + err_msg_minikube)
          end
        end #test_kubectrl_setup
      end
    end
end