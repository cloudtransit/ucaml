require 'minitest/autorun'
require 'ucaml'

module Ucaml
  module Tests

    # Tests the selector implementation of {Ucaml.Transformer#select}.
    #
    # @author Nane Kratzke
    #
    class SelectorTests < Minitest::Test

      # Prepares the test data structure for this test case.
      #
      def setup
        @testdata = {
            a: {
                string: 'value',
                int: 42,
                bool: true,
                float: 3.14,
                hash: {},
                array: []
            },
            b: [
                {
                    string: 'value 1',
                    int: 42,
                    bool: true,
                    float: 3.14,
                    hash: {},
                    array: [
                        {
                            'nested': 'multiple nested values',
                            'other': 'single nested value'
                        }
                    ]
                },
                {
                    string: 'value 2',
                    int: 42,
                    bool: true,
                    float: 3.14,
                    hash: {},
                    array: [
                        {
                            nested: 'multiple nested values'
                        },
                        {
                            nested: 'multiple nested values'
                        }
                    ]
                },
                {
                    string: 'value 3',
                    int: 42,
                    bool: true,
                    float: 3.14
                },
                {
                    string: 'value 4',
                    int: 42,
                    bool: true,
                    float: 3.14,
                    hash: {},
                    array: []
                }
            ]
        }
      end

      # Tests direct selectors returning a single hash.
      #
      def test_direct_hash_selector
        selector = Ucaml::Transformer.new(@testdata,false)
        result = selector.select(@testdata, [:a])
        assert(result.is_a?(Array))
        assert(result.first.is_a?(Hash))
        assert_equal('value', result.first[:string])
        assert_equal(42, result.first[:int])
        assert_equal(true, result.first[:bool])
        assert_equal(3.14, result.first[:float])
        assert_equal({}, result.first[:hash])
        assert_equal([], result.first[:array])
      end

      # Tests direct selectors returning a multiple hashes.
      #
      def test_direct_array_selector
        selector = Ucaml::Transformer.new(@testdata,false)
        result = selector.select(@testdata, [:b])
        assert(result.is_a?(Array))
        assert_equal(4, result.size)
      end

      # Tests diirect selectors returning nothing.
      #
      def test_empty_direct_selector
        selector = Ucaml::Transformer.new(@testdata,false)
        result = selector.select(@testdata, [:missing])
        assert(result.is_a?(Array))
        assert(result.empty?)
      end

      # Tests chained selectors return nothing.
      #
      def test_empty_chained_selector
        selector = Ucaml::Transformer.new(@testdata,false)
        result1 = selector.select(@testdata, [:b, :missing])
        result2 = selector.select(@testdata,[:b, :array, :missing])
        assert(result1.is_a?(Array))
        assert(result1.empty?)
        assert(result2.is_a?(Array))
        assert(result2.empty?)
      end

      # Tests chained selectors returning a single value.
      #
      def test_chained_value_selector
        selector = Ucaml::Transformer.new(@testdata,false)
        result = selector.select(@testdata, [:a, :string])
        assert(result.is_a?(Array))
        assert_equal(1, result.size)
        assert_equal('value', result.first)
      end

      # Tests chained selectors returning multiple values.
      #
      def test_chained_values_selector
        selector = Ucaml::Transformer.new(@testdata, false)

        result1 = selector.select(@testdata, [:b, :int])
        result2 = selector.select(@testdata, [:b, :hash])
        result3 = selector.select(@testdata, [:b, :array])
        result4 = selector.select(@testdata, [:b, :array, :nested])
        result5 = selector.select(@testdata, [:b, :array, :other])


        assert(result1.is_a?(Array))
        assert(result2.is_a?(Array))
        assert(result3.is_a?(Array))
        assert(result4.is_a?(Array))
        assert(result5.is_a?(Array))

        assert_equal(4,result1.size)
        assert_equal(3, result2.size)
        assert_equal(3, result3.size)
        assert_equal(3, result4.size)
        assert_equal(1, result5.size)

        assert(result1.map { |e| e == 42}.all?)
        assert(result2.map { |e| e == {}}.all?)
        assert(result3.map { |e| e.is_a?(Hash)})
        assert(result4.map { |e| e == 'multiple nested values'})
        assert(result5.map { |e| e == 'single nested value'})
      end
    end
  end
end