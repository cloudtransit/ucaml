require 'minitest/autorun'
require 'ucaml'
require 'json'
require 'timeout'
require_relative 'environment/minikube_test'

module Ucaml
  module Tests

    # Tests the K8s transformer {Ucaml.Target.K8s}.
    #
    # @author Peter-Christian Quint and Nane Kratzke
    #
    class TransformerK8sTest < Minitest::Test
      include EnvironmentTest

      # Prepares the test data structures for this test case.
      #
      def setup
          # @testdata = eval(File.read(File.expand_path('../../data/guestbook.ucaml', __FILE__)))
          @prime_service = eval(File.read(File.expand_path('../../data/prime-service.ucaml', __FILE__)))
      end


      # Tests whether the example prime-service application is generated correctly.
      #
      def test_prime_service_manifest_generation
        transformer = Ucaml::Target::K8s.new(@prime_service.build)

        file = Tempfile.new
        file.write(transformer.build)
        file.close

        result = %x{kubectl apply -f #{file.path} --dry-run}
        assert(result.include?("namespace \"#{@prime_service.name}-ns\""))
        assert(result.include?("service \"#{@prime_service.services.first.name}\""))
        assert(result.include?("deployment \"#{@prime_service.services.first.container.name}\""))
        assert(result.include?("horizontalpodautoscaler \"#{@prime_service.services.first.container.name}-scaler\""))

        file.delete
      end

    end
  end
end