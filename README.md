# UCAML - A DSL to Define Cloud Applications in an Unified Format

UCAML (Unified Cloud Application Modeling Language) 
is a domain specific language (DSL) to define 
cloud applications being deployable on elastic container platforms like

- [Kubernetes](https://kubernetes.io/)
- [Swarm](https://docs.docker.com/engine/swarm/)
- [Mesos](https://mesos.apache.org/)/[Marathon](https://mesosphere.github.io/marathon/)
- [Nomad](https://www.nomadproject.io/)
- and more.

UCAML intends to be much more lightweight than other DSL approaches. Even with the 
[simplified TOSCA profile](http://docs.oasis-open.org/tosca/TOSCA-Simple-Profile-YAML/v1.0/os/TOSCA-Simple-Profile-YAML-v1.0-os.html)
one still has to define the infrastructure and platform layer as well.
UCAML's clear focus is to define an [executable architecture](https://en.wikipedia.org/wiki/Executable_architecture) of 
cloud applications without the need to consider complex 
technical details of  platform or infrastructure specifics.

Using UCAML one can define an executable macro architecture 
of a cloud applications in an 
universal definition format and transform this format into
container platform specific definition formats
(see [ISA principles](http://isa-principles.org/) 
to understand the difference between a macro- and a microarchitecture 
of _I_ndependent _S_ystem _A_rchitectures).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ucaml.rb'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ucaml

## Usage

It is recommended to install (minikube)[https://github.com/kubernetes/minikube] 
for your first tests with UCAML.
Furthermore, if you are working with Kubernetes or minikube, 
you need (kubectl)[https://kubernetes.io/docs/tasks/tools/install-kubectl/] installed.

UCAML comes with some preinstalled example cloud applications for your first steps.

- Sock-Shop[https://github.com/microservices-demo/microservices-demo] (a reference cloud application from Weave)
- Guestbook[https://github.com/kubernetes/kubernetes/tree/master/examples/guestbook-go] (A Redis based guestbook example)
- And a very basic prime number checking service. This is UCAMLs version of "hello world".

You can launch this very basic "hello world" cloud application 
(or use the `--sock-shop` to launch the sock shop, or the `--guestbook` to launch the guestbook example)
as follows:

    $ ucaml.rb transform --prime-service > prime-service.yaml
    $ kubectl apply -f prime-service.yaml
    
The first command will generate all necessary manifests for Kubernetes including

- namespace,
- ingress,
- service,
- deployment,
- and even a horizontal pod autoscaler.

The second command will deploy this manifest file (the application definition) to Kubernetes.
After you have started this application you can navigate using your favorite web browser or curl.
Let us assume your K8s cluster can be accessed via 192.168.99.100 (replace with another IP if necessary)

    $ > curl -k http://192.168.99.100:32501/is-prime/127721
    $ Answer will be: 127721 is not a prime number. It can be divided by 11, 17, 187, 683, 7513, 11611
    $
    $ > curl -k http://192.168.99.100:32501/is-prime/127
    $ Answer will be: 127 is a prime number
    
The prime service app is defined via the following definition file.

```Ruby
Ucaml::application('prime-service-app',
  services: [
    Ucaml::service('prime-service',
      request: Ucaml::request(cpu: 100, memory: 256, ephemeral_storage: 2),
      scale: Ucaml::scalingrule(min: 1, max: 3, cpu: 66),
      ports: [80],
      expose: [80 => 32501], #range depends a bit on minikube/k8s config. Default 30001-32767
      container: Ucaml::container('prime-unit', 'transit/primesvc:latest', cmd: ["ruby", "hw-service.rb"])
    )
  ]
)
```

It is possible to write similar files to define the executable master architecture of 
a cloud application and to use `ucaml.rb` to generate Kubernetes manifest files from it.

    $ ucaml.rb transform my-cloud-app.ucaml > my-cloud-app.yaml
    
Right now, UCAML supports 

- Kubernetes and
- Docker Swarm. 

However, it is planned to support further container platforms like 

- Mesos
- Nomad
- ...

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on BitBucket at [https://bitbucket.org/cloudtransit/ucaml](https://bitbucket.org/cloudtransit/ucaml). This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


We are especially looking for the following contributions to support more container platforms.
Check our *how to*'s and learn how to contribute and extend UCAML:

- [HOWTO write a transformer](file.HOWTO_TRANSFORMER.html) to support further container platforms


## License

The gem is available as open source under the terms of the [MIT License](file.LICENSE.html).

## Code of Conduct

Everyone interacting in the Ucaml project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](file.CODE_OF_CONDUCT.html).
