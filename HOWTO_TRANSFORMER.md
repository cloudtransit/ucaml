# HOWTO write a transformer

Transformers process the UCAML format generated via 
the [application](Ucaml.html#application-class_method) generator and transform it
into container platform specific application definition formats 
via a [model transformation](https://en.wikipedia.org/wiki/Model_transformation).


## The general workflow of a model transformation

First of all, it es essential to understand the core concepts of UCAML. UCAML defines cloud applications mainly
with three and very basic concepts.

- An *Application* is composed of *Services*
- A *Service* is deployed as a set of *Containers* 

There are further detail concepts defined by UCAML like scaling rules, scheduling constraints, labels, environment
variabls and so an. But the above mentioned three concepts are the "heart" of UCAML.

https://yuml.me/0e68459b.png

(Edit)[https://yuml.me/edit/0e68459b]

Each UCAML definition is composed of these basic concepts. The workflow of the model to model transformation is working 
like follows. It starts with a cloud application defined in UCAML (`universal:Concept`). This is basically a large nested
hash, that can be accessed via a Concept class in a more convenient way.

https://yuml.me/148c907f.png

(Edit)[https://yuml.me/edit/148c907f]

This nested hash is `preprocess`ed. The preprocessing propagates common settings defined on

- application level down to service level
- and from service level down to container lever.

So `preprocess`ing results in an intermediary nested hash (called `preprocessed`), that contains all settings

- for services on service level and
- for containers on container level

even, if these settings have been defined on higher levels. This is, because it might be useful to define some settings as defaults
in a non redundant way on higher levels. A general
scheduling constraint for an application might be a good example. For instance, to define that an application
should be only hosted on nodes residing in a specific country can be efficiently defined for the whole application
on application level, although it is considered on the lowest container level by most container platforms. Therefore general
settings can be defined in UCAML on higher levels of abstraction. In these cases, these settings are handled as
default values which are being pushed downwards to lower level concepts like containers. This is what 
the `preprocess`ing step is doing. Preprocessing does not contain any specific container platform processing. Preprocessing
is for convenience only and must not be adapted for most target platforms (however it is possible to do this, simply
by overwriting the `preprocess()` method by a transformer).  

Here is how this default preprocessing is implemented in UCAML - it should work for most use cases and container platforms
(do not touch unless you are exactly know what you are doing).

```Ruby
def preprocess(application)

  app = Marshal.load(Marshal.dump(application)) # deep cloning

  # Propagate from application down to services
  for attr in [:labels, :scale, :ports, :environment, :alive, :ready, :scheduling, :request, :volumes]
    self.propagate(app, [], attr, [:services])
  end

  # Propagate from services down to containers
  for attr in [:labels, :scale, :ports, :environment, :alive, :ready, :scheduling, :request, :volumes]
    self.propagate(app, [:services], attr, [:container])
  end

  app
end
```

The last step is the platform specific model transformation via an `build()` method. This method processes the
intermediary hash produced by preprocess and returns a platform specifc definition format. For that step it is helpful
 to understand some basic concepts that might be helpful to develop platform specific transformers.

## Understand some basic concepts

An UCAML cloud application definition is basically nothing more than a large nested hash. 
Transformers process these nested hashs and convert them into a platform specific definition format
like

- manifest files for Kubernetes or
- compose files for Docker Swarm.

To make transformations more convenient to implement, nested hashs can be wrapped by so called `Concept` objects
providing some convenience functions to work with the definition format of UCAML.

For example, using the `Concept` class we can create and read values from nested hashs like that.

```Ruby
concept = Concept.new(attrib1: 'val1', attrib2: ['A', 'short', 'list'], attrib3: { nested: 'value' })
puts(concept.attrib1)        # => val1
puts(concept.attrib2)        # => [A, short, list]
puts(concept.attrib3)        # => Concept(nested: 'value')
puts(concept.attrib3.nested) # => value
# Calling the build method on a concept objects returns a Ruby raw nested hash object
puts(concept.build)          # => { attrib1: 'val1', attrib2: ['A', 'short', list], attrib3: { nested: 'value'}}
```

We can modify objects using the update method.

```
concept = Concept.new(existing: 'value', further: 'VALUE')
concept.update(existing: 'example', new: 'test') # overwrites existing, adds new
puts(concept.build) #  { existing: 'example, further: 'VALUE, new: 'test }
```

And we can use existing concept objects as a kind of template simply by copying and updating them.

```Ruby
template = Concept.new(range: 'default', min: 1, max: 10)
copy = template.copy.update(range: 'my range', max: 100)
puts(template.build) # { range: 'default', min: 1, max: 10 }
puts(copy.build) # { range: 'my range', min: 1, max: 100 }
```

All UCAML concept generators return that kind of `Concept` objects.

```Ruby
default_scaling = Ucaml::scalingrule(min: 1, max: 10, cpu: 75)
aggressive_scaling = default_scaling.copy.update(max: 100, cpu: 25)
puts(default_scaling) # { min: 1, max: 10, cpu: 75 }
puts(aggressive_scaling) # { min: 1, max: 100, cpu: 25 }
```

Furthermore, concept objects provide `as_yaml()` and `as_json()` methods that convert
`Concept` objects into their YAML or JSON string representations. Both formats are heavily used
by container platforms, so even YAML or JSON generation does not have to be implemented by
a transformer class.

So

```Ruby
default_scaling = Ucaml::scalingrule(min: 1, max: 10, cpu: 75)
puts(default_scaling.as_json)
```

would produce the following:

```JSON
{
  "min": 1,
  "max": 10,
  "cpu": 75
}
```

Obviously the same works for YAML.


https://yuml.me/ba0304f6.png

(Edit)[https://yuml.me/edit/ba0304f6]

## The general layout of a transformer

As we see in the class diagram above a transformer extends 
the `Ucaml::Target::Transformer` class and implements the abstract `build()` method
with logic that generates platform specific definition formats. 

The following snippet shows the basic outline of every transformer.

```Ruby
module Ucaml
  module Target
    class SpecificPlatformTransformer < Transformer
      
      def build
        # We have to implement that single method only
      end
      
    end
  end
end
```

All the "magic" happens in this build method. And obviously the magic is comtainer
platform specific.

## Transformers explained by example of Kubernetes 

We explain how a platform specific definition format is generated by explaining the
general outline of the Kubernetes transformer.

### The ``build()`` method

A very general pattern that might be helpful for all transformers is the following.

- To access the preprocessed nested hash as a Concept. This contains the complete setting of the whole application.
- And to select in this object all defined services to process them service by service.

```Ruby
def build
    app = Concept.new(self.preprocessed)
    manifests = [namespace(app)]
    manifests += app.services.map { |svc| [
        service(app, svc),
        deployment(app, svc),
        hpa(app, svc)
    ]}
    manifests.flatten.compact.map { |manifest| manifest.as_yaml } * ''
end
```

Now it depends on the target container platform what has to be done with this setting.
In case of Kubernetes for each service several Kubernetes concepts (manifest) files have to
be generated. The Kubernetes transformer provides these manifests as one big YAML string
that can be processed by the Kubernetes command line interface `kubectl`.

These Kubernetes concepts are:

- a namespace[https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/] manifest for the application to avoid naming collisions between different applications
- a service[https://kubernetes.io/docs/concepts/services-networking/service/] manifest for each service (to define endpoints for each service)
- a deployment[https://kubernetes.io/docs/concepts/workloads/controllers/deployment/] manifest (to deploy containers)
- a [horizontal pod autoscaler](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/) manifest for each container with autoscaling rules applied

All these platform specific generator logic is provided via specific methods. We recommend to
do something similar for other container platforms. 

We do not explain all of these detail concept generating methods in details but focus
on the general outline of these methods. To get all details of these methods we recommend to study the `Ucaml::Target::K8s` transformer class.

### The `namespace()` method (application level concept)

In Kubernertes namespaces provide a scope for names. Names of resources need to be unique within a namespace, but not across namespaces.
Namespaces can be used in Kubernetes to support multiple projects or multiple applications. 

That is exactly how UCAML uses namespaces - to provide unique scopes for different applications. 
Namespaces are generated per application as follows:

```Ruby
def namespace(app)
    Concept.new(
        apiVersion: 'v1',
        kind: 'Namespace',
        metadata: { name: "#{app.name}-ns" }
    )
end
```

The returned *Concept* can be transformed into a valid Kubernetes namespace manifest using the `as_yaml()` method.

### The `service()` method (service level concept)

In Kubernetes a service is an abstraction which defines a logical set of containers (pods) 
and a policy by which to access them - sometimes called a micro-service. 
The set of containers targeted by a service is determined by a label selector.

UCAML selects all containers having the same name being defined under the same service.
Service ports are mapped to service ports according to their definition sequence.
Services are generated per service as follows:

```Ruby
def service(app, svc)
    Concept.new(
        kind: 'Service',
        apiVersion: 'v1',
        metadata: {
            name: svc.name,
            namespace: "#{app.name}-ns"
        },
        spec: {
            selector: {
                container: svc.container.name,
            },
            ports: svc.ports.map.with_index { |port, i| { port: port, targetPort: svc.container.ports[i] }}
        },
    )
end
```

The returned *Concept* can be transformed into a valid Kubernetes service manifest using the `as_yaml()` method.

### The `deployment()` method (container level concept)

In Kubernetes a deployment provides declarative updates for containers (pods).
A desired state is expressed by a deployment object. 
Kubernetes changes the actual state to the desired state at a controlled rate. 

UCAML uses deployments to deploy containers and associates them with mandatory resource limits and requests
to support the scheduler deploying containers (forming services) in a resource optimal way.
Deployments are generated per container of a service as follows:

```Ruby
def deployment(app, svc)
Concept.new(
    apiVersion: 'apps/v1beta2',
    kind: 'Deployment',
    metadata: {
        name: "#{svc.container.name}",
        namespace: "#{app.name}-ns",
    },
    spec: {
        selector: { matchLabels: { container: svc.container.name }},
        template: {
            metadata: { labels: {container: svc.container.name} },
            spec: {
                containers: [
                    {
                        name: svc.container.name,
                        image: svc.container.image,
                        ports: svc.container.ports.map { |p| { containerPort: p }},
                        resources: {
                            limits: { cpu: "#{svc.container.request.cpu}m", memory: "#{svc.container.request.memory}M",  },
                            requests: { cpu: "#{svc.container.request.cpu}m", memory: "#{svc.container.request.memory}M",  }
                        }
                    }
                ]
            }
        }
    }
)
end
```

The returned *Concept* can be transformed into a valid Kubernetes deployment manifest using the `as_yaml()` method.

### The `hpa()` method (container level concept)

In Kubernetes a horizontal pod autoscaler (HPA) automatically scales the number of containers (pods) 
based on observed CPU utilization (or, with custom metrics support, on some other application-provided metrics). 

The HPA periodically adjusts the number of replicas of a container to match the observed average CPU utilization 
to the specified target utilization.
                                                     
UCAML uses HPA to provide auto-scaling of services.
HPA are generated per container with a scaling rule defined as follows:

```Ruby
def hpa(app, svc)
    return nil if svc.container.scale.nil?
    
    Concept.new(
        apiVersion: 'autoscaling/v1',
        kind: 'HorizontalPodAutoscaler',
        metadata: {
            name: "#{svc.container.name}-scaler",
            namespace: "#{app.name}-ns"
        },
        spec: {
            scaleTargetRef: {
                apiVersion: 'apps/v1beta',
                kind: 'Deployment',
                name: svc.container.name
            },
            minReplicas: svc.container.scale.min,
            maxReplicas: svc.container.scale.max,
            targetCPUUtilizationPercentage: svc.container.scale.targetCpu
        }
    )
end
```

The returned *Concept* can be transformed into a valid Kubernetes HPA manifest using the `as_yaml()` method.